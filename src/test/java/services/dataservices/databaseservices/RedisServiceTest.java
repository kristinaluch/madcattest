package services.dataservices.databaseservices;

import com.google.gson.Gson;
import models.Person;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import redis.clients.jedis.Jedis;
import wrappers.WrapperRedis;

import java.util.ArrayList;
import java.util.Set;

import static testdata.basenosqltestdata.RedisTestData.*;

class RedisServiceTest {

    WrapperRedis wrapperRedis = Mockito.mock(WrapperRedis.class);
    Gson gson = Mockito.mock(Gson.class);
    RedisService cut = new RedisService(wrapperRedis, gson);
    Jedis jedis = Mockito.mock(Jedis.class);

    static Arguments[] createTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new Person(1, "N", "M", 12, "T"),
                        "{\"id\":1,\"fname\":\"N\",\"lname\":\"M\",\"age\":12,\"city\":\"T\"}",
                        "1"),
                Arguments.arguments(new Person(5, "Nick", "Marks", 32, "Tbilisi"),
                        "{\"id\":5,\"fname\":\"Nick\",\"lname\":\"Marks\",\"age\":32,\"city\":\"Tbilisi\"}",
                        "5"),
        };
    }

    @ParameterizedTest
    @MethodSource("createTestArgs")
    void createTest(Person person, String jsonPerson, String key){
        Mockito.when(wrapperRedis.getJedis()).thenReturn(jedis);
        Mockito.when(gson.toJson(person)).thenReturn(jsonPerson);
        cut.create(person);
        Mockito.verify(jedis, Mockito.times(1)).append(key, jsonPerson);
    }

    static Arguments[] deleteTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, "1", true),
                Arguments.arguments(10, "10", true),
        };
    }

    @ParameterizedTest
    @MethodSource("deleteTestArgs")
    void deleteTest(int id, String key, boolean expected){
        Mockito.when(wrapperRedis.getJedis()).thenReturn(jedis);

        boolean actual = cut.delete(id);
        Mockito.verify(jedis, Mockito.times(1)).del(key);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] updateTestArgs(){
        return new Arguments[]{
                Arguments.arguments(7, "7", new Person("Sergiy", "Trump", 44, "Dallas"),
                        "{\"id\":7,\"fname\":\"Sergiy\",\"lname\":\"Trump\",\"age\":44,\"city\":\"Dallas\"}",
                        true),
                Arguments.arguments(2, "2", new Person("Ony", "Blade", 25, "California"),
                        "{\"id\":2,\"fname\":\"Ony\",\"lname\":\"Blade\",\"age\":25,\"city\":\"California\"}",
                        true),

        };
    }

    @ParameterizedTest
    @MethodSource("updateTestArgs")
    void updateTest(int id, String key, Person updatePerson, String jsonUpdatePerson, boolean expected){
        Mockito.when(wrapperRedis.getJedis()).thenReturn(jedis);

        Mockito.when(gson.toJson(updatePerson)).thenReturn(jsonUpdatePerson);
        boolean actual = cut.update(id, updatePerson);

        Mockito.verify(jedis, Mockito.times(1)).del(key);

        Mockito.verify(jedis, Mockito.times(1)).append(key, jsonUpdatePerson);

        Assertions.assertEquals(expected, actual);

    }

    static Arguments[] readTestArgs(){
        return new Arguments[]{
                Arguments.arguments(KEYS, "1", "2", "3",
                        "{\"id\":1,\"fname\":\"Mila\",\"lname\":\"Yovovich\",\"age\":32,\"city\":\"Tula\"}",
                        "{\"id\":2,\"fname\":\"Brus\",\"lname\":\"Villis\",\"age\":42,\"city\":\"Zlatoust\"}",
                        "{\"id\":3,\"fname\":\"Korbin\",\"lname\":\"Dallas\",\"age\":55,\"city\":\"Chelyabinsk\"}",
                        PERSON_1, PERSON_2, PERSON_3, TEST_LIST_1),
        };
    }

    @ParameterizedTest
    @MethodSource("readTestArgs")
    void readTest(Set<String> keys, String key1, String key2, String key3,
                  String personJson1, String personJson2, String personJson3,
                  Person person1, Person person2, Person person3, ArrayList<Person> expected){

        Mockito.when(wrapperRedis.getJedis()).thenReturn(jedis);
        Mockito.when(jedis.keys("*")).thenReturn(keys);
        Mockito.when(jedis.get(key1)).thenReturn(personJson1);
        Mockito.when(jedis.get(key2)).thenReturn(personJson2);
        Mockito.when(jedis.get(key3)).thenReturn(personJson3);
        Mockito.when(gson.fromJson(personJson1, Person.class)).thenReturn(person1);
        Mockito.when(gson.fromJson(personJson2, Person.class)).thenReturn(person2);
        Mockito.when(gson.fromJson(personJson3, Person.class)).thenReturn(person3);
        ArrayList<Person> actual = cut.readData();
        Assertions.assertEquals(expected, actual);
    }

}