package services.dataservices.databaseservices;

import com.mongodb.MongoClient;
import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import models.Person;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import wrappers.WrapperMongo;

import java.util.ArrayList;
import java.util.Map;

import static testdata.basenosqltestdata.DataServiceTestData.*;

public class MongoDBServiceTest {

    WrapperMongo wrapperMongo = Mockito.mock(WrapperMongo.class);
    MongoClient mongoClient = Mockito.mock(MongoClient.class);
    MongoDatabase database = Mockito.mock(MongoDatabase.class);
    MongoCollection<Document> mongoCollection = Mockito.mock(MongoCollection.class);
    UpdateResult updateResult = Mockito.mock(UpdateResult.class);
    DeleteResult deleteResult = Mockito.mock(DeleteResult.class);
    FindIterable<Document> iteratorDoc = Mockito.mock(FindIterable.class);
    MongoCursor it = Mockito.mock(MongoCursor.class);

    String nameDatabase = "mongoDB";
    String nameCollection = "persons";


    MongoDBService cut = new MongoDBService(wrapperMongo);

    static Arguments[] createTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new Person(1, "N", "M", 12, "T"), 1,
                        new Document(Map.of("_id", 1, "name", "N", "lname", "M",
                                "age", 12, "city", "T"))),
                Arguments.arguments(new Person(4, "Nnnnnn", "M", 12, "T"), 1,
                        new Document(Map.of("_id", 4, "name", "Nnnnnn", "lname", "M",
                                "age", 12, "city", "T")))

        };
    }

    @ParameterizedTest
    @MethodSource("createTestArgs")
    void createTest(Person person, int mockitoTimes, Document insertDocument){

        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);

        cut.create(person);
        Mockito.verify(mongoCollection, Mockito.times(mockitoTimes)).insertOne(insertDocument);
    }

    static Arguments[] updateTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, new Person(1, "N", "M", 12, "T"), new Document("_id", 1),
                        new Document(Map.of("$set",
                                new Document(Map.of("name","N", "lname", "M",
                                        "age", 12, "city", "T")))), true),
                Arguments.arguments(4, new Person(4, "N", "M", 12, "T"), new Document("_id", 4),
                        new Document(Map.of("$set",
                                new Document(Map.of("name","N", "lname", "M",
                                        "age", 12, "city", "T")))), true)

        };
    }

    @ParameterizedTest
    @MethodSource("updateTestArgs")
    void updateTest(int id, Person updatePerson,
                    Document searchDoc, Document updateDocument, boolean expected){


        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);


        boolean actual = cut.update(id, updatePerson);
        Mockito.when(mongoCollection.updateOne(searchDoc, updateDocument)).thenReturn(updateResult);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] deleteTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, new Document("_id", 1), true),
                Arguments.arguments(4, new Document("_id", 4), true)

        };
    }

    @ParameterizedTest
    @MethodSource("deleteTestArgs")
    void deleteTest(int id, Document deleteDoc, boolean expected){


        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);


        boolean actual = cut.delete(id);
        Mockito.when(mongoCollection.deleteOne(deleteDoc)).thenReturn(deleteResult);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] readDataTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, true, true, false, DOCUMENT_1, DOCUMENT_2, DOCUMENT_3,
                        TEST_LIST_1),
                Arguments.arguments(true, true, false, false, DOCUMENT_1, DOCUMENT_2, null,
                        TEST_LIST_2),
                Arguments.arguments(false, false, false, false, null, null, null,
                        new ArrayList<Person>())
        };
    }

    @ParameterizedTest
    @MethodSource("readDataTestArgs")
    void readDataTest(boolean hasNext1, boolean hasNext2,
                      boolean hasNext3, boolean hasNext4, Document document1, Document document2,
                      Document document3,
                      ArrayList<Person> expected){

        Mockito.when(wrapperMongo.getMongoClient()).thenReturn(mongoClient);
        Mockito.when(mongoClient.getDatabase(nameDatabase)).thenReturn(database);
        Mockito.when(database.getCollection(nameCollection)).thenReturn(mongoCollection);
        Mockito.when(mongoCollection.find()).thenReturn(iteratorDoc);
        Mockito.when(iteratorDoc.iterator()).thenReturn(it);
        Mockito.when(it.hasNext()).thenReturn(hasNext1).
                thenReturn(hasNext2).thenReturn(hasNext3).thenReturn(hasNext4);
        Mockito.when(it.next()).thenReturn(document1).thenReturn(document2).thenReturn(document3);


        ArrayList<Person> actual = cut.readData();

        Assertions.assertEquals(expected, actual);
    }




}
