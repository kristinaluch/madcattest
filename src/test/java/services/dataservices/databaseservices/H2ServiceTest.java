package services.dataservices.databaseservices;

import models.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.sql.*;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class H2ServiceTest {

    private static final String DB_URL = "jdbc:h2:~/test";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    private static final String INSERT_SCRIPT = "INSERT INTO people VALUES (?, ?, ?, ?, ?);";
    private static final String READ_SCRIPT = "SELECT * FROM people;";
    private static final String UPDATE_SCRIPT = "UPDATE people SET fname = ?, lname = ?, age = ?, city = ? WHERE id = ?;";
    private static final String DELETE_SCRIPT = "DELETE FROM people WHERE id = ?;";

    private final Connection connection = Mockito.mock(Connection.class);
    private final PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
    private final ResultSet resultSet = Mockito.mock(ResultSet.class);
    private final Person personTest = new Person(123, "F1", "L1", 20, "C1");

    private final H2Service cut = new H2Service();

    @Test
    void createTest() {

        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(INSERT_SCRIPT)).thenReturn(preparedStatement);

            cut.create(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.getId());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.getFname());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, personTest.getLname());
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(4, personTest.getAge());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, personTest.getCity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void createNullPerson() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager
                    .getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(INSERT_SCRIPT))
                    .thenReturn(preparedStatement);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertDoesNotThrow(() -> cut.create(null));
        Mockito.verifyNoInteractions(preparedStatement);
        System.out.println("create null p was Tested");
    }

    @Test
    void update() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(UPDATE_SCRIPT)).thenReturn(preparedStatement);

            cut.update(123,  personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setString(1, personTest.getFname());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.getLname());
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(3, personTest.getAge());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(4, personTest.getCity());
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(5, personTest.getId());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void delete() {
        try(MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(DELETE_SCRIPT)).thenReturn(preparedStatement);

            cut.delete(11);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1,11);
            Mockito.verify(preparedStatement, Mockito.times(1)).execute();

        } catch (SQLException e){
            e.printStackTrace();
        }
        System.out.println("delete was tested");
    }

    static List<Arguments> readDataTestArgs() {
        return List.of(
                Arguments.arguments(true, 1, 2, 1, 1, 1, 1, 1),
                Arguments.arguments(false, 1, 1, 0, 0, 0, 0, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("readDataTestArgs")
    void readDataTest(boolean next, int executeQueryTimes, int nextTimes, int idTimes,
                      int fNameTimes, int lNameTimes, int ageTimes, int cityTimes) {
        try (MockedStatic<DriverManager> mockDriverManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriverManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(READ_SCRIPT)).thenReturn(preparedStatement);
            Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
            Mockito.when(resultSet.next()).thenReturn(next).thenReturn(false);

            cut.readData();

            Mockito.verify(preparedStatement, Mockito.times(executeQueryTimes)).executeQuery();
            Mockito.verify(resultSet, Mockito.times(nextTimes)).next();
            Mockito.verify(resultSet, Mockito.times(idTimes)).getInt("id");
            Mockito.verify(resultSet, Mockito.times(fNameTimes)).getString("fname");
            Mockito.verify(resultSet, Mockito.times(lNameTimes)).getString("lname");
            Mockito.verify(resultSet, Mockito.times(ageTimes)).getInt("age");
            Mockito.verify(resultSet, Mockito.times(cityTimes)).getString("city");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}