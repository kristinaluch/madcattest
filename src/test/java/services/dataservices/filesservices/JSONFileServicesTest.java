package services.dataservices.filesservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import services.dataservices.filesservices.JSONFileServices;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static testdata.filetestdata.FileServicesTestData.*;

public class JSONFileServicesTest {

    ObjectMapper mapperJSON = Mockito.mock(ObjectMapper.class);

    File fileJSON = new File("src/main/java/files/jsonFile.json");
    JSONFileServices cut = new JSONFileServices(fileJSON, mapperJSON);

    File emptyFile = new File("src/test/java/testdata/filetestdata/EmptyFile.json");
    JSONFileServices cut2 = new JSONFileServices(emptyFile, mapperJSON);

    static Arguments[] readFileTestArgs(){
        return new Arguments[]{
                Arguments.arguments(ARRAY_LIST1, PERSONS_ARRAY_1),
                Arguments.arguments(ARRAY_LIST2, PERSONS_ARRAY_2),
                };
    }


    @ParameterizedTest
    @MethodSource("readFileTestArgs")
    void readFileTest(ArrayList<Person> expected, Person[] readList) throws IOException {
       Mockito.when(mapperJSON.readValue(fileJSON, Person[].class)).thenReturn(readList);

        ArrayList<Person> actual = cut.readFile();

        Assertions.assertEquals(expected, actual);
    }



    @Test
    void readFileIfNullTest() throws IOException {
        //Assertions.assertDoesNotThrow(() ->cut2.readFile());
        ArrayList<Person> actual = cut2.readFile();
        Mockito.verifyNoInteractions(mapperJSON);
        Assertions.assertEquals(new ArrayList<Person>(), actual);
    }


    static Arguments[] writeFileTestArgs(){
        return new Arguments[]{
                Arguments.arguments(ARRAY_LIST1),
                Arguments.arguments(ARRAY_LIST2),
        };
    }


    @ParameterizedTest
    @MethodSource("writeFileTestArgs")
    void writeFileTest(ArrayList<Person> listPerson) throws IOException {

        cut.writeFile(listPerson);
        Mockito.verify(mapperJSON, Mockito.times(1)).writeValue(fileJSON, listPerson);
    }

    @Test
    void writeFileIfNullTest() throws IOException {
        //Assertions.assertDoesNotThrow(() ->cut2.readFile());
        ArrayList<Person> arrayList = null;
        cut2.writeFile(arrayList);
        Mockito.verifyNoInteractions(mapperJSON);
    }



}
