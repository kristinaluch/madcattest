package services.dataservices.filesservices;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import models.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static testdata.filetestdata.FileServicesTestData.*;

public class XMLFileServicesTest {

    XmlMapper mapperXml = Mockito.mock(XmlMapper.class);

    File fileXML = new File("src/main/java/files/xmlFile.xml");
    XMLFileServices cut = new XMLFileServices(fileXML, mapperXml);

    File emptyFile = new File("src/test/java/testdata/filetestdata/EmptyFile.xml");
    XMLFileServices cut2 = new XMLFileServices(emptyFile, mapperXml);

    static Arguments[] readFileTestArgs(){
        return new Arguments[]{
                Arguments.arguments(ARRAY_LIST1, PERSONS_ARRAY_1),
                Arguments.arguments(ARRAY_LIST2, PERSONS_ARRAY_2),
                };
    }


    @ParameterizedTest
    @MethodSource("readFileTestArgs")
    void readFileTest(ArrayList<Person> expected, Person[] readList) throws IOException {

        Mockito.when(mapperXml.readValue(fileXML, Person[].class)).thenReturn(readList);
        ArrayList<Person> actual = cut.readFile();

        Assertions.assertEquals(expected, actual);
    }



    @Test
    void readFileIfNullTest(){
        ArrayList<Person> actual = cut2.readFile();
        Mockito.verifyNoInteractions(mapperXml);
        Assertions.assertEquals(new ArrayList<Person>(), actual);
    }

    static Arguments[] writeFileTestArgs(){
        return new Arguments[]{
                Arguments.arguments(ARRAY_LIST1),
                Arguments.arguments(ARRAY_LIST2),
        };
    }

    @ParameterizedTest
    @MethodSource("writeFileTestArgs")
    void writeFileTest(ArrayList<Person> listPerson) throws IOException {
        cut.writeFile(listPerson);
        Mockito.verify(mapperXml, Mockito.times(1)).writeValue(fileXML, listPerson);
    }

    @Test
    void writeFileIfNullTest(){
        ArrayList<Person> arrayList = null;
        cut2.writeFile(arrayList);
        Mockito.verifyNoInteractions(mapperXml);
    }



}
