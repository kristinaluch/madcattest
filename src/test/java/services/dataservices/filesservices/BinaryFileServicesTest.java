package services.dataservices.filesservices;

import models.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import wrappers.ObjectStreamWrapper;

import java.io.*;
import java.util.ArrayList;

import static testdata.filetestdata.FileServicesTestData.*;


class BinaryFileServicesTest {

    File file = new File("src/main/java/files/binaryFile.bin");

    ObjectStreamWrapper objectStreamWrapper = Mockito.mock(ObjectStreamWrapper.class);
    ObjectInputStream ois = Mockito.mock(ObjectInputStream.class);
    FileInputStream fileInputStream = Mockito.mock(FileInputStream.class);
    ObjectOutputStream oos = Mockito.mock(ObjectOutputStream.class);
    FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);

    BinaryFileServices cut = new BinaryFileServices(file, objectStreamWrapper);



    static Arguments[] readFileTestArgs(){
        return new Arguments[]{
                Arguments.arguments(ARRAY_LIST1, ARRAY_LIST1),
                //Arguments.arguments(ARRAY_LIST2, PERSONS_ARRAY_2),
        };
    }


    @ParameterizedTest
    @MethodSource("readFileTestArgs")
    void readFileTest(ArrayList<Person> expected, ArrayList<Person> personList) throws IOException, ClassNotFoundException {
        Mockito.when(objectStreamWrapper.getObjectInputStream(file)).thenReturn(ois);
        Mockito.when(ois.readObject()).thenReturn(personList);

        ArrayList<Person> actual = cut.readFile();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void writeFile() {
    }


}