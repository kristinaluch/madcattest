package testdata.filetestdata;

import models.Person;

import java.util.ArrayList;
import java.util.List;

public class FileServicesTestData {

    private static final Person PERSON_1 = new Person(1, "Fname1", "Lname1", 1, "Tula");
    private static final Person PERSON_2 = new Person(2, "Fname2", "Lname2", 2, "Tula");
    private static final Person PERSON_3 = new Person(3, "Fname3", "Lname3", 3, "Tula");

    private static final Object OBJECT_1 = new Person(1, "Fname1", "Lname1", 1, "Tula");
    private static final Object OBJECT_2 = new Person(2, "Fname2", "Lname2", 2, "Tula");
    private static final Object OBJECT_3 = new Person(3, "Fname3", "Lname3", 3, "Tula");


    public static final Person[] PERSONS_ARRAY_1 = new Person[]{PERSON_1, PERSON_2, PERSON_3};
    public static final Person[] PERSONS_ARRAY_2 = new Person[]{PERSON_1, PERSON_2};


    public static final ArrayList<Person> ARRAY_LIST1 = new ArrayList<>(List.of(PERSONS_ARRAY_1));
    public static final ArrayList<Person> ARRAY_LIST2 = new ArrayList<>(List.of(PERSONS_ARRAY_2));


    //public static final Object OBJECT_LIST_1 = ARRAY_LIST1;
}
