package testdata.basenosqltestdata;

import models.Person;

import java.util.*;

public class RedisTestData {
    public static final ArrayList<Person> TEST_LIST_1 = new ArrayList<>();

    public static final Person PERSON_1 = new Person(1, "Mila", "Yovovich", 32, "Tula");
    public static final Person PERSON_2 = new Person(2, "Brus", "Villis", 42, "Zlatoust");
    public static final Person PERSON_3 = new Person(3, "Korbin", "Dallas", 55, "Chelyabinsk");

    public static final Set <String> KEYS = (Set<String>) new HashSet<String>();
    public static final String key1 = "1";
    public static final String key2 = "2";
    public static final String key3 = "3";

    static {
        KEYS.add(key1);
        KEYS.add(key2);
        KEYS.add(key3);
        TEST_LIST_1.add(PERSON_1);
        TEST_LIST_1.add(PERSON_2);
        TEST_LIST_1.add(PERSON_3);
    }

}
