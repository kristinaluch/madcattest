package testdata.basenosqltestdata;

import models.Person;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataServiceTestData {




    public static final ArrayList<Person> TEST_LIST_1 = new ArrayList<>();
    public static final ArrayList<Person> TEST_LIST_2 = new ArrayList<>();



    public static final Person PERSON_1 = new Person(1, "Fname1", "Lname1", 1, "Tula");
    public static final Person PERSON_2 = new Person(2, "Fname2", "Lname2", 2, "Tula");
    public static final Person PERSON_3 = new Person(3, "Fname3", "Lname3", 3, "Tula");


    public static final Document DOCUMENT_1 = new Document(Map.of("_id", 1,
            "name", "Fname1", "lname", "Lname1",
            "age", 1, "city", "Tula"));
    public static final Document DOCUMENT_2 = new Document(Map.of("_id", 2,
            "name", "Fname2", "lname", "Lname2",
            "age", 2, "city", "Tula"));
    public static final Document DOCUMENT_3 = new Document(Map.of("_id", 3,
            "name", "Fname3", "lname", "Lname3",
            "age", 3, "city", "Tula"));

    static{
        TEST_LIST_1.add(PERSON_1);
        TEST_LIST_1.add(PERSON_2);
        TEST_LIST_1.add(PERSON_3);

        TEST_LIST_2.add(PERSON_1);
        TEST_LIST_2.add(PERSON_2);
    }


}
