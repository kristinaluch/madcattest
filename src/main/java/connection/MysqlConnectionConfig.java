package connection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnectionConfig {

    private static Logger logger = LogManager.getLogger(MysqlConnectionConfig.class);
    private static final String DB_URL = "jdbc:mysql://localhost/crud";
    private static final String USER = "root";
    private static final String PASSWORD = "rootpassword";

    public static Connection getConnectionToMysql() throws FailedConnectionException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException ex) {
            logger.info("MySql FAIL to connect");
            logger.error(ex.getMessage());

            throw new FailedConnectionException(ex.getMessage());
        }
    }
}


