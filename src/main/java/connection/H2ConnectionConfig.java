package connection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2ConnectionConfig {

    private static Logger logger = LogManager.getLogger(H2ConnectionConfig.class);
    private static final String DB_URL = "jdbc:h2:~/test";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    public static Connection getConnectionToH2() throws FailedConnectionException {
        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException ex) {
            logger.info("h2 FAIL to connect");
            logger.error(ex.getMessage());
            throw new FailedConnectionException(ex.getMessage());
        }
    }
}


