package connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnectionConfig {

    private static Logger logger = LogManager.getLogger(JdbcConnectionConfig.class);
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/crud"; // crud название db
    private static final String USER = "postgres";                                // postgres имя пользователя владельца db
    private static final String PASSWORD = "1";                    // пароль клиента postgres

    public static Connection getConnectionToPostgres() throws FailedConnectionException {

        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);

        } catch (SQLException | ClassNotFoundException e) {
            logger.info("Postgre FAIL to connect");
            logger.error(e.getMessage());
            throw new FailedConnectionException(e.getMessage());

        }
    }
}
