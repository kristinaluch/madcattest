package utils;

import connection.JdbcConnectionConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.IOException;

public class CreateFile {
    private static Logger logger = LogManager.getLogger(JdbcConnectionConfig.class);

    public File createFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.info("Problem with creating file");
                logger.error(e.getMessage());
            }
        }
        return file;
    }
}
