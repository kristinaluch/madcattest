
import caches.IDGeneration;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.gson.Gson;
import gui.SwingTable;
import org.apache.logging.log4j.*;
import services.FactoryService;
import services.dataservices.databaseservices.*;
import services.dataservices.filesservices.*;
import utils.CreateFile;
import wrappers.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.File;
import java.util.Properties;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {

        CreateFile createFile = new CreateFile();
        File binaryFile = createFile.createFile("src/main/java/files/binaryFile.bin");
        File csvFile = createFile.createFile("src/main/java/files/csvFile.csv");
        File jsonFile = createFile.createFile("src/main/java/files/jsonFile.json");
        File xmlFile = createFile.createFile("src/main/java/files/xmlFile.xml");
        File yamlFile = createFile.createFile("src/main/java/files/yamlFile.yaml");

        WrapperMongo wrapperMongo = new WrapperMongo();
        ObjectStreamWrapper objectStreamWrapper = new ObjectStreamWrapper();
        BufferedWrapper bufferedWrapper = new BufferedWrapper();
        ObjectMapper mapperJSON = new ObjectMapper();
        XmlMapper mapperXml = new XmlMapper();
        ObjectMapper mapperYAML = new ObjectMapper(new YAMLFactory());
        WrapperRedis wrapperRedis = new WrapperRedis();
        Gson gson = new Gson();


        FactoryService[] factories = new FactoryService[10];

        factories[0] = new BinaryFileServices(binaryFile, objectStreamWrapper);
        factories[1] = new CSVFileServices(csvFile, bufferedWrapper);
        factories[2] = new JSONFileServices(jsonFile, mapperJSON);
        factories[3] = new XMLFileServices(xmlFile, mapperXml);
        factories[4] = new YAMLFileServices(yamlFile, mapperYAML);
        factories[5] = new H2Service();
        factories[6] = new MongoDBService(wrapperMongo);
        factories[7] = new MySqlService();
        factories[8] = new PostgreSQLService();
        factories[9] = new RedisService(wrapperRedis, gson);

        Properties prop = new Properties();
        FileStreamWrapper fileStreamWrapper = new FileStreamWrapper();
        IDGeneration idGeneration = new IDGeneration(prop, fileStreamWrapper);

        JFrame frame = new JFrame("MadCats");
        JPanel panel = new JPanel();
        JLabel lblName = new JLabel("FName:");
        JLabel lblName_1 = new JLabel("LName:");
        JLabel lblContact = new JLabel("Age:");
        JLabel lblCourse = new JLabel("City");

        JTextField fname = new JTextField();
        JTextField lname = new JTextField();
        JTextField age = new JTextField();
        JTextField city = new JTextField();
        JTable table = new JTable();
        JScrollPane scrollPane = new JScrollPane();
        DefaultTableModel model = new DefaultTableModel();
        JButton btnAdd = new JButton("Add");
        JButton btnDelete = new JButton("Delete");
        JButton btnUpdate = new JButton("Update");
        JButton btnClear = new JButton("Clear");

        String[] dataServicesNameArray = {"binary", "csv", "json", "xml", "yaml", "H2", "MongoDB", "MySql", "PostgreSQL", "Redis"};
        JComboBox btnRead = new JComboBox(dataServicesNameArray);

        EventQueue.invokeLater(() -> {
            try {
                SwingTable window = new SwingTable(factories, idGeneration, frame, panel,
                        lblName, lblName_1, lblContact, lblCourse, fname,
                        lname, age,
                        city, table, scrollPane,
                        model, btnAdd, btnDelete, btnUpdate, btnClear, btnRead);
                window.getFrame().setVisible(true);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        });


    }


}
