package caches;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wrappers.FileStreamWrapper;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class IDGeneration {

    private static final String PATH_TO_PROPERTIES = "src/main/java/resources/config.properties";
    private static Logger logger = LogManager.getLogger(IDGeneration.class);
    private Properties prop;
    private FileStreamWrapper fileStreamWrapper;
    private String param;

    public IDGeneration(Properties prop, FileStreamWrapper fileStreamWrapper) {
        this.prop = prop;
        this.fileStreamWrapper = fileStreamWrapper;
    }

    public void setParam(String param) {
        this.param = param;
    }


    public int getID() {
        try (FileInputStream fileInputStream = fileStreamWrapper.getFileInputStream(PATH_TO_PROPERTIES)) {
            prop.load(fileInputStream);
            int id = Integer.parseInt(prop.getProperty(param));
            setNextId(param, id);
            return id;
        } catch (IOException e) {
            logger.info("������ � ���������: ���� " + PATH_TO_PROPERTIES + " �� ����������");
            logger.error(e.getMessage());
            return -1;
        }
    }

    private void setNextId(String param, int id) {
        String nextID = Integer.toString((id + 1));

        try (FileInputStream fileInputStream = fileStreamWrapper.getFileInputStream(PATH_TO_PROPERTIES)) {
            FileOutputStream outputStream = fileStreamWrapper.getFileOutputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);
            prop.setProperty (param, nextID);
            prop.store(outputStream, "");
            outputStream.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.info("Error nextId");
        }
    }
}
