package services;

import models.Person;
import java.util.ArrayList;

public interface FactoryService {

    void create(Person person);

    boolean update(int id, Person updatePerson);

    boolean delete(int id);

    ArrayList<Person> readData();
}
