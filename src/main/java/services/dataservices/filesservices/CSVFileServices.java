package services.dataservices.filesservices;

import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FactoryService;
import wrappers.BufferedWrapper;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVFileServices extends AbstractFileServices implements FactoryService {

    private static Logger logger = LogManager.getLogger(CSVFileServices.class);
    private File file;
    private BufferedWrapper bufferedWrapper;

    public CSVFileServices(File file, BufferedWrapper bufferedWrapper) {
        this.file = file;
        this.bufferedWrapper = bufferedWrapper;
    }

    @Override
    public ArrayList<Person> readFile() {
        if (file.length() == 0) {
            new ArrayList<Person>();
        }
        List<Person> result = new ArrayList<>();

        try (BufferedReader br = bufferedWrapper.getBufferedReader(file)) {

            HeaderColumnNameMappingStrategy<Person> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Person.class);

            CsvToBean<Person> csvToBean = new CsvToBeanBuilder<Person>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            result = csvToBean.parse();
        } catch (IOException e) {
            logger.info("Problem with read in csv ");
            logger.error(e.getMessage());
        }

        return (ArrayList<Person>) result;
    }

    @Override
    public void writeFile(ArrayList<Person> listPerson) {
        try (BufferedWriter writer = bufferedWrapper.getBufferedWriter(file)) {

            StatefulBeanToCsv<Person> beanToCsv = new StatefulBeanToCsvBuilder<Person>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(listPerson);
        } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException |
                IOException e) {
            logger.info("Problem with write in csv ");
            logger.error(e.getMessage());
        }
    }
}
