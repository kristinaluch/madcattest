package services.dataservices.filesservices;

import models.Person;
import services.FactoryService;
import java.util.ArrayList;

public abstract class AbstractFileServices implements FactoryService {

    private ArrayList<Person> listPerson;

    abstract ArrayList<Person> readFile();

    abstract void writeFile(ArrayList<Person> listPerson);

    @Override
    public void create(Person person) {
        listPerson = readFile();
        if (listPerson == null) {
            listPerson = new ArrayList<>();
        }
        listPerson.add(person);
        writeFile(listPerson);
    }

    @Override
    public boolean update(int id, Person updatePerson) {
        listPerson = readFile();

        if (listPerson == null) {
            return false;
        }

        System.out.println("in fs" + updatePerson);
        boolean wasUpdated = false;
        for (int i = 0; i < listPerson.size(); i++) {
            Person person = listPerson.get(i);
            System.out.println(person.getId() + " " + i);
            if (person.getId() == updatePerson.getId()) {
                System.out.println("beret p" + person);
                listPerson.set(i, updatePerson);
                System.out.println("vtavlyaet" + listPerson.get(i));
                writeFile(listPerson);
                wasUpdated = true;
                break;
            }
        }
        return wasUpdated;
    }

    @Override
    public boolean delete(int id) {
        listPerson = readFile();
        if (listPerson == null) {
            return false;
        }
        boolean wasDeleted = false;
        for (int i = 0; i < listPerson.size(); i++) {


            // del comm

            Person person = listPerson.get(i);
            System.out.print(person.getId() + " = " + id);
            System.out.println(" ??? ");
            System.out.print(person.getId() == id);
            if (person.getId() == id) {
                listPerson.remove(i);
                writeFile(listPerson);
                wasDeleted = true;
                break;
            }
        }
        System.out.println("deleting result was " + wasDeleted);
        return wasDeleted;
    }

    @Override
    public ArrayList<Person> readData() {
        ArrayList<Person> personArrayList = readFile();
        return personArrayList;
    }

}
