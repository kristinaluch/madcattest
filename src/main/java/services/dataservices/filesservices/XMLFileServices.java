package services.dataservices.filesservices;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FactoryService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class XMLFileServices extends AbstractFileServices implements FactoryService {

    private static Logger logger = LogManager.getLogger(XMLFileServices.class);
    private File file;
    private XmlMapper mapper;

    private final long ROOT_TAG = 7L;

    public XMLFileServices(File file, XmlMapper mapper) {
        this.file = file;
        this.mapper = mapper;
    }


    @Override
    public ArrayList<Person> readFile() {
        if (file.length() <= ROOT_TAG) {
            return new ArrayList<Person>();
        }
        Person[] readList;
        try {
            //��������������
            readList = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            logger.info("Problem with read in xml ");
            logger.error(e.getMessage());
            return new ArrayList<Person>();
        }
        ArrayList<Person> personList = new ArrayList<>(Arrays.asList(readList));
        return personList;
    }

    @Override
    public void writeFile(ArrayList<Person> listPerson) {
        if (listPerson == null) {
            return;
        }
        try {
            mapper.writeValue(file, listPerson);
        } catch (IOException e) {
            logger.info("Problem with write in xml ");
            logger.error(e.getMessage());
        }
    }
}
