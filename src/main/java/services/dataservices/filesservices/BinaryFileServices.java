package services.dataservices.filesservices;


import models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FactoryService;
import wrappers.ObjectStreamWrapper;
import java.io.*;
import java.util.ArrayList;


public class BinaryFileServices extends AbstractFileServices implements FactoryService {

    private static Logger logger = LogManager.getLogger(BinaryFileServices.class);
    private File file;
    private ObjectStreamWrapper objectStreamWrapper;

    public BinaryFileServices(File file, ObjectStreamWrapper objectStreamWrapper) {

        this.file = file;
        this.objectStreamWrapper = objectStreamWrapper;
    }

    @Override
    public ArrayList<Person> readFile() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }

        ArrayList<Person> personList = new ArrayList<>();

        try (ObjectInputStream ois = objectStreamWrapper.getObjectInputStream(file)) {
            personList = (ArrayList<Person>) ois.readObject();
        } catch (Exception e) {
            logger.info("problems with read in binary file");
            logger.error(e.getMessage());
        }

        return personList;

    }


    @Override
    public void writeFile(ArrayList<Person> listPerson) {
        if (listPerson.size() == 0) {
            return;
        }
        try (ObjectOutputStream oos = objectStreamWrapper.getObjectOutputStream(file)) {
            oos.writeObject(listPerson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
