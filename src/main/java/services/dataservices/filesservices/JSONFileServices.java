package services.dataservices.filesservices;


import com.fasterxml.jackson.databind.ObjectMapper;
import models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FactoryService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class JSONFileServices extends AbstractFileServices implements FactoryService {

    private static Logger logger = LogManager.getLogger(JSONFileServices.class);
    private File file;
    private ObjectMapper mapper;

    public JSONFileServices(File file, ObjectMapper mapper) {
        this.file = file;
        this.mapper = mapper;
    }

    @Override
    public ArrayList<Person> readFile() {
        if (file.length() < 3) {
            return new ArrayList<Person>();
        }
        Person[] personsArray;
        try {
            //��������������
            personsArray = mapper.readValue(file, Person[].class);

        } catch (IOException e) {
            logger.info("problem with read in json service");
            logger.error(e.getMessage());
            return new ArrayList<Person>();
        }

        ArrayList<Person> personList = new ArrayList<>(Arrays.asList(personsArray));
        return personList;
    }

    @Override
    public void writeFile(ArrayList<Person> listPerson) {
        if (listPerson == null) {
            return;
        }
        try {
            mapper.writeValue(file, listPerson);
        } catch (IOException e) {
            logger.error(e);
        }

    }
}
