package services.dataservices.filesservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FactoryService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class YAMLFileServices extends AbstractFileServices implements FactoryService {

    private static Logger logger = LogManager.getLogger(YAMLFileServices.class);
    private File file;
    private ObjectMapper mapper;

    public YAMLFileServices(File file, ObjectMapper mapper) {
        this.file = file;
        this.mapper = mapper;
    }

    @Override
    public ArrayList<Person> readFile() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }
        Person[] readList;
        try {
            //��������������
            readList = mapper.readValue(file, Person[].class);

        } catch (IOException e) {
            logger.info("Problem with read in yml ");
            logger.error(e.getMessage());
            return new ArrayList<Person>();
        }
        ArrayList<Person> personList = new ArrayList<>(Arrays.asList(readList));
        return personList;
    }

    @Override
    public void writeFile(ArrayList<Person> listPerson) {
        try {
            mapper.writeValue(file, listPerson);
        } catch (IOException e) {
            logger.info("Problem with write in yml ");
            logger.error(e.getMessage());
        }
    }
}
