package services.dataservices.databaseservices;


import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import models.Person;
import services.FactoryService;
import java.util.ArrayList;

public class CassandraService implements FactoryService {

    private static final String DB_NODE = "127.0.0.1";
    private static final int DB_PORT = 9042;

    private static final String SQL_SELECT_ALL_COUNT = "SELECT count(*) FROM cassandra_keyspace.persons;";
    private static final String SQL_SELECT_ALL = "SELECT * FROM cassandra_keyspace.persons;";
    private static final String SQL_USE_KEYSPACE = "USE cassandra_keyspace;";
    private static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS cassandra_keyspace.persons("
            + "id int PRIMARY KEY, first_name text, last_name text, age int, city text);";

    private Cluster cluster;
    private Session session;
    private String query;

    @Override
    public void create(Person person) {
        connect(DB_NODE, DB_PORT);
        createKeyspace("cassandra_keyspace", "SimpleStrategy", 1);
        createTable();
        insertPerson(person);

        close();
    }

    @Override
    public boolean update(int id, Person updatePerson) {
        connect(DB_NODE, DB_PORT);

        useKeyspace();

        this.query = "UPDATE persons SET first_name = '" + updatePerson.getFname()
                + "', last_name = '" + updatePerson.getLname()
                + "', age = " + updatePerson.getAge()
                + ", city = '" + updatePerson.getCity()
                + "' WHERE id = " + updatePerson.getId() + ";";

        session.execute(query);

        close();
        return true;
    }

    @Override
    public boolean delete(int id) {

        connect(DB_NODE, DB_PORT);

        useKeyspace();

        this.query = "DELETE FROM persons WHERE id = " + id + ";";

        session.execute(query);

        close();

        return true;
    }

    @Override
    public ArrayList<Person> readData() {

        if (getPersonsCount() == 0) {
            return new ArrayList<Person>();
        }

        //Person [] persons;

        connect(DB_NODE, DB_PORT);

        ArrayList<Person> personList;
        if (getPersonsCount() == 0) {
            return new ArrayList<Person>();
        }
        personList = new ArrayList<>(getPersonsCount());
        //persons = new Person[getPersonsCount()];

        this.query = SQL_SELECT_ALL;

        ResultSet result = session.execute(query);

        Row row;

        for (int i = 0; i < getPersonsCount(); i++) {

            row = result.one();

            Person person = new Person();

            person.setId(row.getInt("id"));
            person.setFname(row.getString("first_name"));
            person.setLname(row.getString("last_name"));
            person.setAge(row.getInt("age"));
            person.setCity(row.getString("city"));

            personList.add(person);

//            persons[i] = new Person();
//
//            persons[i].setId(row.getInt("id"));
//            persons[i].setFname(row.getString("first_name"));
//            persons[i].setLname(row.getString("last_name"));
//            persons[i].setAge(row.getInt("age"));
//            persons[i].setCity(row.getString("city"));

        }
        close();

        return personList;
    }

    public void connect(String node, Integer port) {

        Cluster.Builder b = Cluster.builder().addContactPoint(node);

        if (port != null) {

            b.withPort(port);

        }

        cluster = b.build();

        session = cluster.connect();

    }

    public void close() {

        session.close();
        cluster.close();

    }

    public void createKeyspace(String keyspaceName, String replicationStrategy, int replicationFactor) {

        this.query = "CREATE KEYSPACE IF NOT EXISTS " + keyspaceName + " WITH replication = { 'class':'"
                + replicationStrategy + "','replication_factor':" + replicationFactor + "};";

        session.execute(query);

    }

    public void createTable() {

        this.query = SQL_CREATE_TABLE;

        session.execute(query);
    }

    public void insertPerson(Person person) {

        useKeyspace();

        query = "INSERT INTO persons(id, first_name, last_name, age, city) VALUES ("
                + person.getId() + ", '" + person.getFname() + "', '" + person.getLname() + "', " + person.getAge()
                + ", '" + person.getCity() + "');";
        session.execute(query);
    }


    public void useKeyspace() {

        this.query = SQL_USE_KEYSPACE;

        session.execute(query);

    }

    public int getPersonsCount() {

        this.query = SQL_SELECT_ALL_COUNT;
        System.out.println(query);

        if (session == null) {
            return 0;
        }
        System.out.println(session);
        ResultSet result = session.execute(query);

        long rowsQuantity = result.one().getLong(0);

        return (int)rowsQuantity;

    }
}