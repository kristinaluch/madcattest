package services.dataservices.databaseservices;

import com.google.gson.Gson;
import models.Person;
import redis.clients.jedis.Jedis;
import services.FactoryService;
import wrappers.WrapperRedis;
import java.util.ArrayList;
import java.util.Set;

public class RedisService implements FactoryService {

    private WrapperRedis wrapperRedis;
    private Gson gson;

    public RedisService(WrapperRedis wrapperRedis, Gson gson) {
        this.wrapperRedis = wrapperRedis;
        this.gson = gson;
    }

    @Override
    public void create(Person person) {
        Jedis jedis = wrapperRedis.getJedis();
        String jsonPerson = gson.toJson(person);
        String key = Integer.toString(person.getId());

        jedis.append(key, jsonPerson);
        jedis.close();
    }

    @Override
    public boolean update(int id, Person updatePerson) {
        Jedis jedis = wrapperRedis.getJedis();

        String key = Integer.toString(id);
        jedis.del(key);
        String jsonUpdatePerson = gson.toJson(updatePerson);
        jedis.append(key, jsonUpdatePerson);
        jedis.close();
        return true;

    }

    @Override
    public boolean delete(int id) {
        Jedis jedis = wrapperRedis.getJedis();

        String key = Integer.toString(id);
        jedis.del(key);
        jedis.close();

        return true;
    }

    @Override
    public ArrayList<Person> readData() {
        Jedis jedis = wrapperRedis.getJedis();
        ArrayList<Person> personsList = new ArrayList<>();
        Set<String> keys = jedis.keys("*");
        Object[] keysObj = keys.toArray();
        for (Object key : keysObj) {
            String keyStr = key.toString();
            String personJson = jedis.get(keyStr);
            Person person = gson.fromJson(personJson, Person.class);
            personsList.add(person);
        }
        jedis.close();
        return personsList;
    }
}
