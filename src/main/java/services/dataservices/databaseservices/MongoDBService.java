package services.dataservices.databaseservices;


import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import models.Person;
import org.bson.Document;
import services.FactoryService;
import wrappers.WrapperMongo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class MongoDBService implements FactoryService {

    private WrapperMongo wrapperMongo;
    private MongoDatabase database;
    private MongoCollection<Document> mongoCollection;

    private final String NAME_DATABASE = "mongoDB";
    private final String NAME_COLLECTION = "persons";
    private final String ID = "_id";
    private final String NAME = "name";
    private final String LNAME = "lname";
    private final String AGE = "age";
    private final String CITY = "city";

    public MongoDBService(WrapperMongo wrapperMongo) {
        this.wrapperMongo = wrapperMongo;
    }

    @Override
    public void create(Person person) {

        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);

            Document document = new Document(Map.of(ID, person.getId(),
                    NAME, person.getFname(), LNAME, person.getLname(),
                    AGE, person.getAge(), CITY, person.getCity()));

            mongoCollection.insertOne(document);
        }
    }

    @Override
    public boolean update(int id, Person updatePerson) {

        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);

            Document searchDoc = new Document(ID, id);

            mongoCollection.updateOne(searchDoc, new Document(Map.of("$set",
                    new Document(Map.of(NAME, updatePerson.getFname(), LNAME, updatePerson.getLname(),
                            AGE, updatePerson.getAge(), CITY, updatePerson.getCity())))));
        }
        return true;
    }

    @Override
    public boolean delete(int id) {
        if (id <= 0) {
            return false;
        }

        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);

            mongoCollection.deleteOne(new Document(ID, id));
        }

        return true;
    }

    @Override
    public ArrayList<Person> readData() {
        ArrayList<Person> personList;
        try (MongoClient mongoClient = wrapperMongo.getMongoClient()) {
            database = mongoClient.getDatabase(NAME_DATABASE);
            mongoCollection = database.getCollection(NAME_COLLECTION);
            Document readDocument;
            Person readPerson;

            FindIterable<Document> iteratorDoc = mongoCollection.find();
            Iterator it = iteratorDoc.iterator();

            personList = new ArrayList<>();

            while (it.hasNext()) {
                readDocument = (Document) it.next();
                readPerson = new Person();

                readPerson.setId(Integer.parseInt(readDocument.get(ID).toString()));
                readPerson.setFname(readDocument.get(NAME).toString());
                readPerson.setLname((readDocument.get(LNAME).toString()));
                readPerson.setAge(Integer.parseInt(readDocument.get(AGE).toString()));
                readPerson.setCity(readDocument.get(CITY).toString());
                personList.add(readPerson);
            }
            return personList;
        }


    }
}
