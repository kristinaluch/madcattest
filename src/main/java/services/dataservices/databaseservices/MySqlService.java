package services.dataservices.databaseservices;

import connection.FailedConnectionException;
import connection.MysqlConnectionConfig;
import models.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FactoryService;
import java.sql.*;
import java.util.ArrayList;

public class MySqlService implements FactoryService {

    private static Logger logger = LogManager.getLogger(MySqlService.class);
    private static final String TABLE_CONFIG = "CREATE TABLE IF NOT EXISTS people (" +
            "id int8 NOT NULL PRIMARY KEY, " +
            "fname CHARACTER VARYING(1500), " +
            "lname CHARACTER VARYING(1500) NOT NULL, " +
            "age int8 NOT NULL, city CHARACTER VARYING(168) NOT NULL," +
            "CONSTRAINT people_age_check CHECK(age >0 AND age < 200)" +
            ");";
    private static final String INSERT_SCRIPT = "INSERT INTO people VALUES (?, ?, ?, ?, ?);";
    private static final String DELETE_SCRIPT = "DELETE FROM people WHERE id = ?;";
    private static final String UPDATE_SCRIPT = "UPDATE people SET fname = ?, lname = ?, age = ?, city = ? WHERE id = ?;";
    private static final String READ_SCRIPT = "SELECT * FROM people;";

    public MySqlService() {
        createTable();
    }

    @Override
    public void create(Person person) {

        if (person == null) {
            return;
        }

        try (
                Connection connection = MysqlConnectionConfig
                        .getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(INSERT_SCRIPT)
        ) {
            ps.setInt(1, person.getId());
            ps.setString(2, person.getFname());
            ps.setString(3, person.getLname());
            ps.setInt(4, person.getAge());
            ps.setString(5, person.getCity());
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            logger.error(e.getMessage());
        }

    }

    @Override
    public boolean update(int id, Person updatePerson) {
        try (
                Connection connection = MysqlConnectionConfig.getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(UPDATE_SCRIPT)
        ) {
            ps.setString(1, updatePerson.getFname());
            ps.setString(2, updatePerson.getLname());
            ps.setInt(3, updatePerson.getAge());
            ps.setString(4, updatePerson.getCity());
            ps.setInt(5, updatePerson.getId());
            ps.execute();
            return true;
        } catch (SQLException | FailedConnectionException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        try (
                Connection connection = MysqlConnectionConfig.getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(DELETE_SCRIPT)
        ) {
            ps.setInt(1, id);
            ps.execute();
            return true;
        } catch (SQLException | FailedConnectionException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public ArrayList<Person> readData() {
        ArrayList<Person> people = new ArrayList<>();
        try (
                Connection connection = MysqlConnectionConfig.getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(READ_SCRIPT)
        ) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Person newPerson = new Person();

                int id = resultSet.getInt("id");
                String fname = resultSet.getString("fname");
                String lname = resultSet.getString("lname");
                int age = resultSet.getInt("age");
                String city = resultSet.getString("city");

                newPerson.setId(id);
                newPerson.setFname(fname);
                newPerson.setLname(lname);
                newPerson.setAge(age);
                newPerson.setCity(city);

                people.add(newPerson);
            }
        } catch (SQLException | FailedConnectionException e) {
            logger.error(e.getMessage());
        }

        return people;
    }

    private void createTable() {
        try (
                Connection connection = MysqlConnectionConfig.getConnectionToMysql();
                Statement statement = connection.createStatement();
        ) {
            statement.executeUpdate(TABLE_CONFIG);
        } catch (SQLException | FailedConnectionException e) {
            logger.error(e.getMessage());
        }
    }
}
