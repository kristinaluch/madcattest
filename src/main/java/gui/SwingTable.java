package gui;

import caches.IDGeneration;
import models.Person;
import services.FactoryService;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class SwingTable {

    private JFrame frame;
    private final JLabel lblName;
    private final JLabel lblName_1;
    private final JLabel lblContact;
    private final JLabel lblCourse;
    private final JTextField fname;
    private final JTextField lname;
    private final JTextField age;
    private final JTextField city;
    private final JTable table;
    private final JScrollPane scrollPane;
    private DefaultTableModel model;
    private JComboBox btnRead;
    private final JButton btnAdd;
    private JButton btnDelete;
    private JButton btnUpdate;
    private JButton btnClear;
    private JPanel panel;
    private final Object[] row = new Object[5];
    private IDGeneration idGeneration;
    private FactoryService[] factories;
    private final String MSG_Fill_COMPLETE_INFO = "Please Fill Complete Info";
    private final String MSG_AGE_RANGE_ERROR =
            "Incorrect age! Minimum age 0 years, maximum 120 years";
    private final String MSG_AGE_NOT_NUMBER =
            "Incorrect age! Age must be a number";
    private final String MSG_INCORRECT_FNAME =
            "Incorrect firstname! First name must contain only letters";
    private final String MSG_INCORRECT_LNAME =
            "Incorrect last name! Last name must contain only letters";
    private final String MSG_INCORRECT_CITY =
            "Incorrect city! City must contain only letters or hyphen";
    private final String MSG_SELECT_THE_ROW =
            "Please Select a Row First";
    private final String MSG_EXIT = "Do you want really close app";
    private final String EMPTY_FIELD = "";
    private final String MSG_ARE_YOU_SURE = "Are you sure?";
    private final String CONFIRMATION_WINDOW = "Confirmation window";
    private final String WRONG_SYMBOLS = "[^0-9`\\s\\-~!@#$%^&*()_+=?>\n<,.;/:{}\"]{1,20}";
    private final String REGEX_MUST_TO_BE = "(" + WRONG_SYMBOLS + "[- ]{0,1})?(" + WRONG_SYMBOLS + ")";


    private List<Person> listOfPerson = new ArrayList<>();

    public JFrame getFrame() {
        return frame;
    }

    public SwingTable(FactoryService[] factories, IDGeneration idGeneration,
                      JFrame frame, JPanel panel, JLabel lblName, JLabel lblName_1,
                      JLabel lblContact, JLabel lblCourse, JTextField fname,
                      JTextField lname, JTextField age,
                      JTextField city, JTable table, JScrollPane scrollPane,
                      DefaultTableModel model,
                      JButton btnAdd,
                      JButton btnDelete, JButton btnUpdate, JButton btnClear,
                      JComboBox btnRead
    ) {
        this.factories = factories;
        this.idGeneration = idGeneration;
        this.frame = frame;
        this.panel = panel;
        this.lblName = lblName;
        this.lblName_1 = lblName_1;
        this.lblContact = lblContact;
        this.lblCourse = lblCourse;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.city = city;
        this.table = table;
        this.scrollPane = scrollPane;
        this.model = model;
        this.btnAdd = btnAdd;
        this.btnUpdate = btnUpdate;
        this.btnDelete = btnDelete;
        this.btnClear = btnClear;
        this.btnRead = btnRead;

        initialize();
    }


    private void initialize() {
        setFrameParam();
        setPanelParam();
        setLabelParam();
        setTextFieldParam();
        setScrollPaneParam();
        setTableParam();
        setModelParam();
        setBtnAddParam();
        setBtnReadParam();
        setBtnDeleteParam();
        setBtnUpdateParam();
        setBtnClearParam();
    }


    private void setFrameParam() {
        frame.setResizable(false);
        frame.setBounds(100, 100, 775, 437);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int x = JOptionPane.showConfirmDialog(null,
                        MSG_EXIT, "Close",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.INFORMATION_MESSAGE);
                if (x == JOptionPane.YES_OPTION) {
                    e.getWindow().dispose();
                }
            }
        });
        frame.getContentPane().setLayout(null);
    }


    private void setPanelParam() {
        panel.setBackground(new Color(176, 196, 222));
        panel.setBounds(0, 0, 767, 410);
        frame.getContentPane().add(panel);
        panel.setLayout(null);
    }

    private void setLabelParam() {
        lblName.setBounds(21, 83, 46, 14);
        panel.add(lblName);

        lblName_1.setBounds(21, 106, 46, 14);
        panel.add(lblName_1);

        lblContact.setBounds(21, 127, 46, 14);
        panel.add(lblContact);

        lblCourse.setBounds(21, 148, 46, 14);
        panel.add(lblCourse);
    }

    private void setTextFieldParam() {
        fname.setBounds(67, 81, 132, 17);
        fname.setColumns(10);
        panel.add(fname);

        lname.setColumns(10);
        lname.setBounds(67, 102, 132, 17);
        panel.add(lname);

        age.setColumns(10);
        age.setBounds(67, 124, 132, 17);
        panel.add(age);

        city.setColumns(10);
        city.setBounds(67, 145, 132, 17);
        panel.add(city);
    }

    private void setScrollPaneParam() {
        scrollPane.setBounds(224, 48, 517, 339);
        panel.add(scrollPane);
    }

    private void setTableParam() {
        table.addMouseListener(
                new MouseAdapter() {
                    @Override
                    public void mouseClicked(
                            MouseEvent e) {
                        super.mouseClicked(e);
                        int i = table.getSelectedRow();

                        fname.setText(model.getValueAt(i, 1).toString());
                        lname.setText(model.getValueAt(i, 2).toString());
                        age.setText(model.getValueAt(i, 3).toString());
                        city.setText(model.getValueAt(i, 4).toString());

                        if (Integer.parseInt(age.getText()) < 0 |
                                Integer.parseInt(age.getText()) > 120) {
                            JOptionPane
                                    .showMessageDialog(null, MSG_AGE_RANGE_ERROR);
                            model.setValueAt(listOfPerson.get(i).getAge(), i,
                                    3);
                            return;
                        }

                        if (checkWord(String.valueOf(fname))) {
                            refreshPerson(i);
                        }

                        if (!listOfPerson.get(i).getFname()
                                .equals(fname.getText()) ||
                                !listOfPerson.get(i).getLname()
                                        .equals(lname.getText()) ||
                                !listOfPerson.get(i).getCity()
                                        .equals(city.getText()) ||
                                listOfPerson.get(i).getAge() !=
                                        Integer.parseInt(age.getText())) {
                            if (0 == JOptionPane.showConfirmDialog(null,
                                    MSG_ARE_YOU_SURE, CONFIRMATION_WINDOW,
                                    JOptionPane.OK_CANCEL_OPTION)) {
                                listOfPerson.get(i).setFname(fname.getText());
                                listOfPerson.get(i).setLname(lname.getText());
                                listOfPerson.get(i).setAge(Integer.parseInt(age.getText()));
                                listOfPerson.get(i).setCity(city.getText());
                                factories[btnRead.getSelectedIndex()]
                                        .update(i, listOfPerson.get(i));

                            } else {
                                refreshPerson(i);

                            }
                            model.setValueAt(listOfPerson.get(i).getId(), i, 0);

                        }
                    }
                });
        table.setBackground(new Color(240, 248, 255));
    }

    private void setModelParam() {
        Object[] column = {"Id", "FName", "LName", "Age", "City"};

        model.setColumnIdentifiers(column);
        table.setModel(model);
        scrollPane.setViewportView(table);
    }

    private void setBtnAddParam() {
        btnAdd.addActionListener(e -> {
            String fnameString = fname.getText();
            String lnameString = lname.getText();
            String ageString = age.getText();
            String cityString = city.getText();

            String errorMSG =
                    checkField(fnameString, lnameString, ageString, cityString);

            if (errorMSG != null) {
                JOptionPane.showMessageDialog(null, errorMSG);
                return;
            } else {
                int id = idGeneration.getID();
                if (id < 0) {
                    return;
                }
                Person personAdd = new Person(id, fnameString,
                        lnameString, Integer.parseInt(ageString), cityString);
                listOfPerson.add(personAdd);
                factories[btnRead.getSelectedIndex()]
                        .create(personAdd);
                row[0] = personAdd.getId();
                row[1] = fnameString;
                row[2] = lnameString;
                row[3] = ageString;
                row[4] = cityString;
                model.addRow(row);
                fname.setText(EMPTY_FIELD);
                lname.setText(EMPTY_FIELD);
                age.setText(EMPTY_FIELD);
                city.setText(EMPTY_FIELD);
                JOptionPane.showMessageDialog(null, "Saved Successfully");
            }
        });

        btnAdd.setBounds(21, 280, 80, 23);
        panel.add(btnAdd);
    }

    private void refreshPerson(int i) {
        model.setValueAt(listOfPerson.get(i).getId(), i, 0);
        model.setValueAt(listOfPerson.get(i).getFname(), i, 1);
        model.setValueAt(listOfPerson.get(i).getLname(), i, 2);
        model.setValueAt(listOfPerson.get(i).getAge(), i, 3);
        model.setValueAt(listOfPerson.get(i).getCity(), i, 4);
    }

    private void setBtnReadParam() {
        btnRead.setBounds(21, 236, 180, 23);
        panel.add(btnRead);
        btnRead.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btnRead) {
                    model.setRowCount(0);
                    listOfPerson = factories[btnRead.getSelectedIndex()]
                            .readData();
                    idGeneration.setParam(
                            String.valueOf(btnRead.getSelectedItem()));

                    for (int i = 0; i < listOfPerson.size(); i++) {
                        row[0] = listOfPerson.get(i).getId();
                        row[1] = listOfPerson.get(i).getFname();
                        row[2] = listOfPerson.get(i).getLname();
                        row[3] = listOfPerson.get(i).getAge();
                        row[4] = listOfPerson.get(i).getCity();
                        model.addRow(row);
                    }
                }
            }
        });

    }

    private void setBtnDeleteParam() {
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table.getSelectedRow();

                if (i >= 0) {

                    if (0 == JOptionPane
                            .showConfirmDialog(null, MSG_ARE_YOU_SURE,
                                    CONFIRMATION_WINDOW,
                                    JOptionPane.OK_CANCEL_OPTION)) {
                        factories[btnRead.getSelectedIndex()]
                                .delete(listOfPerson.get(i).getId());
                        model.removeRow(i);
                        listOfPerson.remove(i);
                        JOptionPane.showMessageDialog(null,
                                "Deleted Successfully");
                    }

                } else {
                    JOptionPane.showMessageDialog(null,
                            MSG_SELECT_THE_ROW);
                }
            }
        });

        btnDelete.setBounds(21, 324, 80, 23);
        panel.add(btnDelete);
    }

    private void setBtnUpdateParam() {
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table.getSelectedRow();

                if (i >= 0) {
                    String fnameString = fname.getText();
                    String lnameString = lname.getText();
                    String ageString = age.getText();
                    String cityString = city.getText();

                    model.setValueAt(fnameString, i, 1);
                    model.setValueAt(lnameString, i, 2);
                    model.setValueAt(ageString, i, 3);
                    model.setValueAt(cityString, i, 4);

                    String errorMSG =
                            checkField(fnameString, lnameString, ageString,
                                    cityString);
                    if (errorMSG != null) {
                        JOptionPane.showMessageDialog(null, errorMSG);
                        refreshPerson(i);
                        return;
                    }
                    if (0 != JOptionPane
                            .showConfirmDialog(null, MSG_ARE_YOU_SURE,
                                    CONFIRMATION_WINDOW,
                                    JOptionPane.OK_CANCEL_OPTION)) {

                        return;
                    }

                    JOptionPane.showMessageDialog(null,
                            "Updated Successfully");

                    listOfPerson.get(i).setFname(fnameString);
                    listOfPerson.get(i).setLname(lnameString);
                    listOfPerson.get(i).setAge(Integer.parseInt(ageString));
                    listOfPerson.get(i).setCity(cityString);
                    Person updatePerson = listOfPerson.get(i);
                    int id = updatePerson.getId();
                    factories[btnRead.getSelectedIndex()]
                            .update(id, updatePerson);
                } else {
                    JOptionPane.showMessageDialog(null,
                            MSG_SELECT_THE_ROW);
                }
            }
        });
        btnUpdate.setBounds(123, 280, 80, 23);
        panel.add(btnUpdate);
    }

    private void setBtnClearParam() {
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fname.setText(EMPTY_FIELD);
                lname.setText(EMPTY_FIELD);
                age.setText(EMPTY_FIELD);
                city.setText(EMPTY_FIELD);
            }
        });
        btnClear.setBounds(123, 324, 80, 23);
        panel.add(btnClear);
    }

    private boolean checkWord(String str) {

        boolean ifCorrect = str.matches(REGEX_MUST_TO_BE);
        return ifCorrect;
    }

    private String checkField (String fnameString, String lnameString, String ageString, String cityString) {
        if (fnameString.equals(EMPTY_FIELD) || lnameString.equals(EMPTY_FIELD) ||
                ageString.equals(EMPTY_FIELD) || cityString.equals(EMPTY_FIELD)) {
            return MSG_Fill_COMPLETE_INFO;
        }

        boolean correctAge = ageString.matches("[\\d]{1,3}");
        if (!correctAge) {
            return MSG_AGE_NOT_NUMBER;
        }

        int ageInt = Integer.parseInt(ageString);
        if (ageInt > 120 || ageInt < 0) {
            return MSG_AGE_RANGE_ERROR;
        }

        boolean correctFname = fnameString.matches(REGEX_MUST_TO_BE);
        boolean correctLname = lnameString.matches(REGEX_MUST_TO_BE);
        boolean correctCity = cityString.matches(REGEX_MUST_TO_BE);

        if (!correctFname) {
            return MSG_INCORRECT_FNAME;
        }
        if (!correctLname) {
            return MSG_INCORRECT_LNAME;
        }
        if (!correctCity) {
            return MSG_INCORRECT_CITY;
        }

        return null;
    }

}
