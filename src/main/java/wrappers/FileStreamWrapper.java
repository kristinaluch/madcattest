package wrappers;

import java.io.*;

public class FileStreamWrapper {

    public FileInputStream getFileInputStream(String fileName) throws IOException {
        return new FileInputStream(fileName);
    }

    public FileOutputStream getFileOutputStream(String fileName) throws IOException {
        return new FileOutputStream(fileName);
    }

}
