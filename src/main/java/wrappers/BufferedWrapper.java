package wrappers;

import java.io.*;

public class BufferedWrapper {

    public BufferedReader getBufferedReader(File file) throws IOException {
        return new BufferedReader(new FileReader(file));
    }

    public BufferedWriter getBufferedWriter(File file) throws IOException {
        return new BufferedWriter(new FileWriter(file));
    }
}
